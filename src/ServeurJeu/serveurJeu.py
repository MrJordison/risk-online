#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Déroulement partie:

    établissement connexion de chaque client, validation bla bla ... Vérifie connexion n'est pas déjà existante...

    Attente des pseudo de chaque joueur sous la forme: "pseudo:ojrtgorjigto" . Vérifie que que le pseudo n'est pas pris,
    partie n'est pas déjà lancée.
    A chaque joueur connecté, incrémente le compteur du nb de joueur.
    
    Dès que le seuil minimal (ici 3) de joueurs est tatin (comme la tarte),
    le chef de partie (premier joueur à s'être connecté), décide de lancer la partie quand il le souhaite
    avec message : "game:start"

    Génération des territoires, points d'armée de joueurs, couleur associée et territoire de départ(en aléatoire)

    Envoi d'un top départ à chaque client qui génère une interface du gameboard vierge (même message de retour en tcp "game:start")
    puis envoie de toutes les données à chaque client (map, joueurs,etc...) (format json de la mongodb) qui mettra à jour l'interface
    
    Entame le déroulement des tours de jeu

    Tour de jeu:
    Chaque client envoie son mouvement (combat, déplacement) avec comme info (zonedepart,zonevisee, nombre de troupe). le joueur associé peut être recoupé d'après
    la socket client qui reçoit les données sur le serveur. format du message : "move:zone_depart:zone_visee:nombre_troupes".
    Si mouvement valide, serveur envoie confirmation "move_status:accepted", sinon "move_status:denied:error_message". Affichage dans l'interface client du type de     de retour pour confirmer. Possibilité d'annuler mouvement avec move_status:cancel" de la part du client. Supprimera le mouvement dans l'historique de mongodb.

    lorsque tous les joueurs ont envoyé leur mouvement, serveur envoie message "tour:end" ou equiv pour prévenir clients. Ensuite applique les mouvements l'un après l'autre selon ordre des joueurs (ordre de connexion par exemple). Effectue la résolution de chaque mouvement (combat, perte des unités si besoin et attribution ouveaux territoires de chaque joueur). Chaque modification sera enregistrée dans mongodb puis envoyée à tous les clients (format selon mongodb, identique à la création de la map) .
    
    Fin de tour :
    après résolutions mouvement, attribution de nouvelles unités, etc ... Et envoie message "tour:new" afin de prévenir clients et déblocage interface de jeu (sécurité si besoin). Effectue aussi les tests de perdants / gagnants : si un joueur n'a plus de territoire / armée : déclaré perdant avec message "game:loose" de la part du serveur. Bloque interface (plus d'envoi depuis client perdant) mais recevra toujours les maj du jeu (mode spectate). On décrémente un compteur qui gère le nombre de joueurs dont on attend un mouvement durant le tour actuel. Dans le cas où le compteur est égal à 1, on regarde le joueur restant qui sera donc gagnant. Le serveur lui enverra le message "game:win" pour popup dans l'itnerface puis envoie à tous les clients "game:end" afin de clore le jeu et les connexions.
    
    Améliorations possibles :
    - gestion déconnexion / reconnexion des joueurs (suppression simple si avant jeu démarré, sinon garde un statut déconnecté pendant x temps/tour jusqu'à déclarer abandon)
"""
from MainCommand import MainCommand
from ConnexionManager import ConnexionManager
from CoreGame import CoreGame

main_command = MainCommand(ConnexionManager(8080), CoreGame(True))
