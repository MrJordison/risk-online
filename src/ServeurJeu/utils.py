# -*- coding: utf-8 -*-

import json, random

def format_message(content,type):
    #cas d'erreur
    if type==1:
        entete = "error"
    elif type==2:
        entete = "data"
    return json.dumps({entete:content})+"\n"


def generate_color():
    red = random.randint(0,255)
    green = random.randint(0,255)
    blue = random.randint(0,255)
    res = ("#%02X%02X%02X" %(red,green,blue))
    
    return res
