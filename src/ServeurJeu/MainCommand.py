# -*- coding: utf-8 -*-

import re, socket, ConnexionManager, CoreGame
from utils import format_message
from Observer import GameObserver

def lire_message(connexion_client):
    ligne = ''
    while(1):
        car = connexion_client.recv(1).decode()
        if not car:break
        if car == '\n':
            #ligne+=car
            break
        ligne+=car
    return ligne

class MainCommand:

    def __init__(self, manager_conn, game_instance):
        self.game_instance = game_instance
        self.manager_conn = manager_conn
        self.manager_conn.set_main_command(self)
        self.game_instance.add_observer(GameObserver(self.manager_conn))
        manager_conn.run()

    def error_message(self, message, client_socket, pseudo):
        #envoie le message d'erreur à la connexion persistante du client s'il y en a une
        #sinon directement par la client_socket volatile avant de la fermer
        result = self.manager_conn.send_permanent_client(pseudo,message)
        if not result:
            client_socket.sendall(message.encode())

    def command(self, client_socket, tsap_client):
        message = lire_message(client_socket)

        print(tsap_client,"envoie",message)

        separateur = re.compile(":")
        arguments = separateur.split(message)
        #on vérifie que la commande est correcte (commence par pseudo:MonPseudo:arguments:...
        if not arguments[0] == "pseudo":
            reponse = format_message("Commande incorrecte",1)
            self.error_message(reponse, client_socket, None)
            client_socket.close()
            return 0

        pseudo_joueur = arguments[1]

        if len(arguments) == 2:
            reponse = format_message("Commande requise",1)
            self.error_message(reponse, client_socket, pseudo_joueur)
            client_socket.close()
            return 0

        #un joueur envoie un message pour participer au jeu
        if arguments[2] == "participate":
            #la connexion établie sera la connexion persistante qui enverra les infos depuis le serveur
            self.manager_conn.add_permanent_client((client_socket, tsap_client, pseudo_joueur))
            result = self.game_instance.add_joueur(pseudo_joueur,tsap_client)
            #si le résultat est différent de 0, on renvoie le message d'erreur au client
            if result != 0:
                self.manager_conn.remove_permanent_client((client_socket, tsap_client, pseudo_joueur))
                client_socket.sendall(format_message(result,1).encode())
                client_socket.close()
                return 0

        #un joueur envoie un message pour démarrer la partie
        elif arguments[2] == "game" :
            if len(arguments) != 4:
                result = format_message("commande incomplete ou incorrecte",1)
                self.error_message(result, client_socket, pseudo_joueur)
                client_socket.close()
                return 0

            if arguments[3] == "start":
                result = self.game_instance.start_game(pseudo_joueur)

                if result != 0:
                    self.error_message(format_message(result, 1), client_socket, pseudo_joueur)
                    client_socket.close()
                    return 0

        #un joueur envoie un message pour effectuer un mouvement
        elif arguments[2] == "move":
            if len(arguments) != 6:
                result =  format_message("commande incomplete ou incorrecte",1)
                self.error_message(result, client_socket, pseudo_joueur)
                client_socket.close()
                return 0

            result = self.game_instance.mouvement_joueur(pseudo_joueur,int(arguments[3]), int(arguments[4]), int(arguments[5]))
            print(result)

            if result != 0:
                self.error_message(format_message(result,1), client_socket, pseudo_joueur)
                client_socket.close()
                return 0

        else:
            result = format_message("commande incorrecte",1)
            self.error_message(result, client_socket, pseudo_joueur)
            client_socket.close()
            return 0
