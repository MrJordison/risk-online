# -*- coding: utf-8 -*-

import os, socket, sys, select, MainCommand

class ConnexionManager:

    def __init__(self,port):
        self.port = port
        self.permanent_clients = []
        self.main_command = None

    def run(self):
        #initialisation de la socket serveur
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        server_socket.bind(('',self.port))
        server_socket.listen(socket.SOMAXCONN)

        while(1):
            (events_in, events_out, event_except) = select.select([server_socket], [], [])
            for event in events_in:

                #si event provient du server_socket : c'est une nouvelle connexion d'un client
                if event == server_socket:
                    print("Nouvelle connexion")
                    #récupération de la socket client et le tsap associé
                    (new_client, TSAP_client) = server_socket.accept()
                    self.manage_connexion(new_client,TSAP_client)

    def send_all_permanent_clients(self,message):
        for client in self.permanent_clients :
            client[0].sendall(message.encode())

    def send_permanent_client(self,client_info,message):
        client = self.get_permanent_client(client_info)
        if client != None :
            client[0].sendall(message.encode())
            return True
        return False

    def get_permanent_client(self,client_info):
        #recherche le client selon l'info (tsap ou pseudo utilisé)
        for client in self.permanent_clients:
            if client_info == client[1] or client_info == client[2]:
                return client
        return None

    def add_permanent_client(self,client):
        self.permanent_clients.append(client)

    def remove_permanent_client(self, client):
        self.permanent_clients.remove(client)

    def manage_connexion(self,client_socket, tsap_client):
        self.main_command.command(client_socket,tsap_client)

    def set_main_command(self, command):
        self.main_command = command
