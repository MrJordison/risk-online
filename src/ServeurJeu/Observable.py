# -*- coding: utf-8 -*-

class Observable:
    
    def __init__(self):
        self.observers = []

    def notify(self, msg):
        for observer in self.observers:
            observer.update(msg)

    def add_observer(self, observer):
        self.observers.append(observer)

    def remove_observer(self, observer):
        self.observers.remove(observer)     
