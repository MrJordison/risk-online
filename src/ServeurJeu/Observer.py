# -*- coding: utf-8 -*-

from ConnexionManager import ConnexionManager
from utils import format_message

class Observer:
    
    def __init__(self):
        pass

    def update(self, message):
        pass

class GameObserver(Observer):

    def __init__(self, server):
        Observer.__init__(self)
        self.server = server

    def update(self,message):
        self.server.send_all_permanent_clients(message)
