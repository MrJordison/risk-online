# -*- coding: utf-8 -*-

#Classe noyau du serveur, gère l'instance de Risk

from pymongo import MongoClient, ASCENDING, DESCENDING
import json
from pprint import pprint
import random
from Observable import Observable
from utils import format_message, generate_color


#fonction de résolution de combat entre deux forces armées
#retourne le nombre de troupes des deux camps après
#affrontement
def combat(troupes_atk, troupes_def):
    #lancés de dés pour chaque troupe et tri part ordre décroissant dans chaque liste
    des_atk = sorted([random.randint(1,6) for x in range(troupes_atk)], key=int, reverse = True)
    des_def = sorted([random.randint(1,6) for x in range(troupes_def)], key=int, reverse = True)

    max_comparaison = min(troupes_atk, troupes_def)
    pertes_atk = 0
    pertes_def = 0

    #comparaison des valeurs de dés lancés et attribution de pertes
    for i in range(0,max_comparaison):
        if des_atk[i] <= des_def[i]:
            pertes_atk +=1
        elif des_atk[i] > des_def[i]:
            pertes_def +=1

    return (max(0,troupes_atk - pertes_atk), max(0,troupes_def - pertes_def))


class CoreGame(Observable):

    def __init__(self, is_new = True):
        Observable.__init__(self)
        self.db_connect = MongoClient()
        self.database = self.db_connect['Risk']
        self.map = self.database['map']
        self.joueurs = self.database['joueurs']
        self.mouvements = self.database['mouvements']
        self.proprietes = self.database['proprietes']
        if is_new:
            self.clean_game()
            self.cpt_id = 1
        else:
            self.cpt_id = joueurs.find().sort("id",DESCENDING)[0]["id"]

    #nettoyage de la mongodb et remise au propres de propriétés de la partie
    def clean_game(self):
        self.map.delete_many({})
        self.joueurs.delete_many({})
        self.mouvements.delete_many({})
        self.proprietes.delete_many({})
        self.proprietes.insert({"jeu":"Risk","status":"waiting","joueurs_mini":3,"tour":0,"chef":""})


    def get_proprietes(self):
        return self.proprietes.find_one({"jeu":"Risk"})

    def add_joueur(self, pseudo, tsap_connected):
        #vérifie le statut du jeu : ajout possible seulement avant début partie (mode waiting)
        if self.get_proprietes()["status"] != "waiting":
            return "error:jeu deja lance ou termine"
        #vérifie que le pseudo n'est pas utilisé
        elif self.joueurs.find({"pseudo":pseudo}).count():
            return "error:pseudo deja utilise"
        #vérifie que le tsap n'est pas déjà présent (joueur déjà connecté)
        elif self.joueurs.find({"tsap":tsap_connected}).count():
            return "error:joueur deja connecte"
        else:
            self.joueurs.insert({"pseudo":pseudo,"tsap":tsap_connected,"color":generate_color(),"territoires":0,"troupes":0,"id":self.cpt_id})
            self.notify(format_message(self.get_json_data("joueurs"),2))
            self.cpt_id+=1
            if self.joueurs.find().count() == 1:
                self.proprietes.find_one_and_update({"jeu":"Risk"}, {'$set':{'chef':pseudo}})
            self.notify(format_message(self.get_json_data("proprietes"),2))
            return 0

    def remove_joueur(self,pseudo):
        #vérifie le status du jeu : suppression possible avant le lancement de la partie
        if self.get_proprietes()["status"] != "waiting":
            return "error:partie lancee, pas possible de supprimer un joueur"
        else:
            self.joueurs.delete_one({"pseudo":pseudo})
            self.notify(format_message(self.get_json_data("joueurs"),2))
            return 0

    def start_game(self, pseudo):
        #vérifie le status du jeu : la partie ne peut être lancé que mode "waiting"
        if self.get_proprietes()["status"] != "waiting":
            return "error:partie deja lancee ou terminee"
        #vérifie le nombre minimal de participants atteint
        elif self.get_proprietes()["joueurs_mini"] > self.joueurs.find().count():
            return "error: le nombre minimal de participants n'est pas atteint"
        #vérifie que le joueur qui lance est bien le chef de partie
        elif self.get_proprietes()["chef"] != pseudo:
            return "error: le joueur demandant n'est pas le chef de partie"
        else:
            #met à jour le statut du jeu et initialise la map, les troupes des joueurs et leur territoire
            self.proprietes.find_one_and_update({"jeu":"Risk"}, {"$set":{"status":"running"}})
            self.init_map_joueurs()
            self.notify(format_message(self.get_json_data("all"),2))
            return 0

    def init_map_joueurs(self):
        #chargements des territoires depuis le fichier de données
        with open('territoires.json') as data_file:
            data = json.load(data_file)
        self.map.insert_many(data["territoires"])

        #attribution d'un territoire à chaque joueur et des 10 troupes de départ sur celui ci
        for joueur in self.joueurs.find():
            done = False
            #territoire choisi aléatoirement
            while not done:
                id_territoire = random.randint(1,42)
                if self.map.find_one({"id":id_territoire})["joueur"] == "":
                    done = True
                    self.map.find_one_and_update({"id":id_territoire},{"$set":{"joueur":joueur["pseudo"],"troupes":10}})
                    self.joueurs.find_one_and_update({"pseudo":joueur["pseudo"]},{"$set":{"territoires":1,"troupes":10}})
        #démarrage du compteur de tour de jeu
        self.proprietes.find_one_and_update({"jeu":"Risk"},{"$inc":{"tour":1}})

    def mouvement_joueur(self, pseudo, territoire_depart, territoire_arrivee, nb_troupes):
        #vérifie pseudo du joueur correct
        if not self.joueurs.find({"pseudo":pseudo}).count():
            return "error:joueur non existant"
        #vérifie si le joueur a déjà joué durant ce tour
        elif self.mouvements.find({"joueur":pseudo,"tour":self.get_proprietes()["tour"]}).count():
            return "error: une action a deja ete effectuee durant le tour"
        #cas où un joueur veut passer son tour, envoie -1 comme territoires et troupes
        elif territoire_depart == territoire_arrivee == nb_troupes == -1 :
            self.mouvements.insert({"joueur":pseudo,"territoire_depart":territoire_depart,"territoire_arrivee":territoire_arrivee,"troupes":nb_troupes,"tour":self.get_proprietes()["tour"]})

        #vérifie si les territoires existent
        elif (not self.map.find({"id":territoire_depart}).count()) or (not self.map.find({"id":territoire_arrivee}).count()):
            return "error: un des territoires n'existe pas"
        #vérifie si le territoire de départ appartient au joueur
        elif not self.map.find({"id":territoire_depart,"joueur":pseudo}).count():
            return "error:le territoire n'appartient pas au joueur specifie"
        #vérifie si le nombre de troupes est correct (strict supérieur à 0)
        elif (nb_troupes <= 0):
            return "error: nombre de troupes specifie incorrect(doit etre positif)"
        #vérifie si le nombre de troupes est correct (strict inférieur au nb troupes du territoires et à 10)
        elif (nb_troupes > 10) or (nb_troupes >= self.map.find_one({"id":territoire_depart})["troupes"]):
            return "error: nombre de troupes specifie incorrect (ne doit pas depasser 10 ou le total du territoire)"
        #vérifie que le territoire visé est adjacent à celui de départ
        elif not territoire_arrivee in self.map.find_one({"id":territoire_depart})["adjacents"]:
            return "error: territoire vise non adjacent a celui de depart"
        else:
            #ajout du mouvement au tour actuel dans la mongodb
            self.mouvements.insert({"joueur":pseudo,"territoire_depart":territoire_depart,"territoire_arrivee":territoire_arrivee,"troupes":nb_troupes,"tour":self.get_proprietes()["tour"]})

        #si tous les joueurs ont effectué leur action, on met fin au tour actuel et on effectue la résolution des déplacements / combats
        if self.mouvements.find({"tour":self.get_proprietes()["tour"]}).count() == self.joueurs.find().count():
            self.resolution_tour()
            self.notify(format_message(self.get_json_data("all"),2))
        return 0

    def resolution_tour(self):

        #on récupère la liste des joueurs encore présents en jeu, on résout leur mouvement selon leur ordre de connexion (représenté par l'id)
        j_actifs = [joueur for joueur in self.joueurs.find().sort("id", ASCENDING)]
        print(j_actifs)

        #on résout le mouvement de chaque joueur dans l'ordre de connexion (représenté par l'id de chaque joueur)
        i = 0
        while i < len(j_actifs):

            mouvement = self.mouvements.find_one({"joueur":j_actifs[i]["pseudo"],"tour":self.get_proprietes()["tour"]})

            #cas où un joueur a passé son tour
            if not mouvement["territoire_depart"] == mouvement["territoire_arrivee"] == mouvement["troupes"] == -1 :
                #maj du territoire de départ, décrémente le nombre de troupes déplacées
                self.map.find_one_and_update({"id":mouvement["territoire_depart"]},{"$inc":{"troupes":-mouvement["troupes"]}})

                territoire_arrivee = self.map.find_one({"id":mouvement["territoire_arrivee"]})

                #cas territoire visé non possédé ou neutre : le joueur le conquiert directement
                if territoire_arrivee["joueur"] == "":
                    self.map.find_one_and_update({"id":mouvement["territoire_arrivee"]},{"$set":{"joueur":mouvement["joueur"],"troupes":mouvement["troupes"]}})
                
                #cas territoire déjà possédé, juste un mouvement de troupes qui s'additionnent à celles déjà en place
                elif territoire_arrivee["joueur"] == mouvement["joueur"]:
                    self.map.find_one_and_update({"id":mouvement["territoire_arrivee"]},{"$inc":{"troupes":mouvement["troupes"]}})

                #cas où le territoire est possédé par un joueur : résolution par combat d'après les règles officielles du Risk
                #lancer de dé pour chaque troupe de chaque joueur et comparaison par ordre décroissant : le joueur ayant le dé le plus petit a chaque comparaison
                #perd une troupe engagée. En cas d'égalité sur une comparaison le défenseur l'emporte
                else:
                    troupes_atk = mouvement["troupes"]
                    troupes_def = self.map.find_one({"id":mouvement["territoire_arrivee"]})["troupes"]

                    (new_troupes_atk, new_troupes_def) = combat(troupes_atk, troupes_def)

                    if new_troupes_def == 0:
                        self.map.find_one_and_update({"id":mouvement["territoire_arrivee"]},{"$set":{"joueur":mouvement["joueur"],"troupes":new_troupes_atk}})

                    else:
                        self.map.find_one_and_update({"id":mouvement["territoire_depart"]},{"$inc":{"troupes":new_troupes_atk}})
                        self.map.find_one_and_update({"id":mouvement["territoire_arrivee"]},{"$set":{"troupes":new_troupes_def}})

                    #mise à jour du joueur défenseur et regarde si il est éliminé
                    if not self.maj_joueur_stats(territoire_arrivee["joueur"]) :
                        j_actifs = [j for j in j_actifs if j["pseudo"]!=territoire_arrivee["joueur"]]

                #mise à jour du joueur attaquant
                self.maj_joueur_stats(j_actifs[i]["pseudo"])
            i+=1

        print("Fin de la résolution des combats")

        #on regarde si il reste plus d'un participant pour terminer la partie ou non
        if self.joueurs.find().count() != 1:
            #on incrémente le compteur de tours de jeu
            self.proprietes.find_one_and_update({"jeu":"Risk"},{"$inc":{"tour":1}})
            self.gain_tour()

        #il ne reste qu'un joueur, c'est donc le gagnant et la partie est terminée
        else:
            self.proprietes.find_one_and_update({"jeu":"Risk"},{"$set":{"status":"ended"}})
            print("fin du jeu")

    def maj_joueur_stats(self, joueur):

        #maj du nombre de troupes et de territoires du joueur dans la collection "joueur"
        troupes = 0
        liste_territoires = self.map.find({"joueur":joueur})
        territoires = liste_territoires.count()
        for territoire in liste_territoires:
            troupes+= territoire["troupes"]

        #si le joueur n'a plus de territoires / troupes, il est donc éliminé du jeu
        if (troupes == 0) or (territoires == 0):
            self.joueurs.delete_one({"pseudo":joueur})
            return 0

        #sinon, on met à jour la collection "joueurs" en fonction des infos récupérées
        else:
            self.joueurs.find_one_and_update({"pseudo":joueur},{"$set":{"troupes":troupes,"territoires":territoires}})
            return 1

    def gain_tour(self):
        #selon règles basique du Risk, un joueur gagne autant de troupes qu'il a de multiple de 3 territoires + 1
        #on ajoute une troupe en iterant autant de fois qu'il faut sur les territoires qu'il possède
        for joueur in self.joueurs.find():
            new_troupes = int(joueur["territoires"] / 3) + 1
            self.joueurs.find_one_and_update({"pseudo":joueur["pseudo"]},{"$inc":{"troupes":new_troupes}})
            territoires_liste = [t["id"] for t in self.map.find({"joueur":joueur["pseudo"]})]
            for i in range(new_troupes):
                self.map.find_one_and_update({"id":territoires_liste[(i%len(territoires_liste))]},{"$inc":{"troupes":1}})

    def get_json_data(self, type_data):
        res = dict()
        if type_data == "territoires" or type_data == "all":
            territoires = []
            for t in self.map.find():
                del t['_id']
                territoires.append(t)
            res["territoires"] = territoires

        if type_data == "joueurs" or type_data == "all":
            joueurs = []
            for j in self.joueurs.find():
                del j['_id']
                del j['tsap']
                joueurs.append(j)
            res["joueurs"] = joueurs

        if type_data == "proprietes" or type_data == "all":
            proprietes = self.get_proprietes()
            del proprietes['_id']
            res["proprietes"] = proprietes

        return res
