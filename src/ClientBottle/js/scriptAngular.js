var app = angular.module("AppRisk", ['ngRoute']);

app.config(["$routeProvider", function($routeProvider){

    $routeProvider
        .when("/", { //route par defaut amenant a l'ecran de connexion
            controller: "ControllerLogin as loginControl",
            templateUrl: "template/login.html"
        })
        .when("/gameWaiting", { //route pour l'ecran d'attente
            controller: "ControllerWaiting as waitingControl",
            templateUrl: "template/gameWaiting.html"
        })
        .when("/game", { //route pour l'ecran de jeu
            controller: "ControllerGame as gameControl",
            templateUrl: "template/gameScreen.html"
        })
        .otherwise({
            redirectTo:"/"
        });
}]);
