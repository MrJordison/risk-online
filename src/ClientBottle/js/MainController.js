//controleur de l'ecran de connexion
app.controller("ControllerLogin", ["$scope", "$location", function($scope, $location){
    var loginControl = this;

    //permet de tenter une connexion au serveur lorsqu'on a saisi son pseudo
    loginControl.login = function(){
        var pseudoSaisi = loginControl.pseudoSaisi;

        connexionPermanente(pseudoSaisi);
    }

    //si la connexion au serveur a reussi on met le joueur sur la page d'attente
    loginControl.connexion = function(){
        $location.path("/gameWaiting");
    }
}]);

//controleur de l'ecran d'attente
app.controller("ControllerWaiting", ["$scope", "$location", function($scope, $location){

    var waitingControl = this;

    //On met a jour le nombre de joueurs en attente
    updateNbJoueurs();

    //si on clique sur le bouton pour commencer la partie on envoie la requete au serveur
    waitingControl.buttonStart = function(){
        gameStart();
    }

    //si le serveur a lance la partie on met le joueur sur la page de jeu
    waitingControl.startGame = function(){
        $location.path("/game")
    }

}]);

//controleur de la page de jeu
app.controller("ControllerGame", ["$scope", "$location", function($scope, $location){

    var gameControl = this;

    gameControl.joueurs = listJoueurs;

    gameControl.joueurs.forEach(function(j){
        if(j.pseudo == pseudo)
            j.indiquation = "#A30000";
        else
            j.indiquation = "black";
    });
    console.log(gameControl.joueurs);

}]);
