var pseudo = "";
var isConnect = false;
var gameWaiting = true;
var isLeader = false;

var minNbJoueurs = -1;
var nbJoueurs = 0;
var listJoueurs = [];
var listTerritoires = [];

var zoneDepart = -1;
var zoneArrivee = -1;
var hasPlayed = false;

var game;

//Permet de récupérer le controller angular
function getController(){
    return scopeAng = angular.element(document.querySelector("body")).scope().$$childHead;
}

//Permet de mettre a jour le nombre de joueurs connecte
function updateNbJoueurs(){

    //Si on est en train d'attendre le lancement de la partie
    if(gameWaiting){

        $("#gameWaitingCounter").html("Joueurs Connectés : "+nbJoueurs+"<br/> En attente d'autres joueurs");

        //Si le joueur est le leader et si on a atteint le nombre de joueurs min on affiche le bouton pour lancer la partie
        if(isLeader && minNbJoueurs != -1 && minNbJoueurs <= nbJoueurs){
            $("#gameWaitingStartButton").show();
        }
        else{
            $("#gameWaitingStartButton").hide();
        }
    }

}

//Appele pour chaque message du serveur
function onMessageArrive(message){

    //On recupere en format json le message du serveur
    var arguments = jQuery.parseJSON(message)["message"];

    //On recupere le message en entete (afin de savoir si il s'agit d'une erreur ou non)
    var entete = Object.keys(arguments)[0];

    //Si c'est une erreur on l'affiche
    if(entete == "error"){
        alert(arguments.error);
    }
    else{

        //On recupere les differentes categories du contenu du message
        var infoNames = Object.keys(arguments.data);

        //Si on a des informations sur les joueurs
        if(infoNames.indexOf("joueurs") != -1){

            //On met à jour le nombre de joueurs
            nbJoueurs = arguments.data.joueurs.length;

            //On met à jour la liste des joueurs
            listJoueurs = arguments.data.joueurs;

            //Si le joueur est pas connecte
            if(!isConnect){

                //Si on a pas eu de message d'erreur c'est que le jeu n'est pas commence donc on l'accepte
                isConnect = true;

                //On indique le pseudo du joueur
                pseudo = $("#pseudo").val();

                //On indique au serveur qu'on veut se connecter sur la page d'attente du jeu
                getController().loginControl.connexion();
                getController().$apply();

            }
            else{

                //on met a jour les champs de joueurs
                updateNbJoueurs();
                var perdu = true;

                listJoueurs.forEach(function(j){
                    if(j.pseudo == pseudo)
                        perdu = false;
                });

                if(perdu){
                    alert("Vous avez perdu !");
                }

            }

        }

        //Si on a des informations sur l'etat du jeu
        if(infoNames.indexOf("proprietes") != -1){

            var proprietes = arguments.data.proprietes;

            //si le jeu est en attente
            if(proprietes.status == "waiting"){

                //On verifie qui est le leader/chef du jeu
                if(proprietes.chef == pseudo){
                    isLeader = true;
                }

                //Et on recupere le nombre minimum de joueurs re
                minNbJoueurs = proprietes.joueurs_mini;

            }
            else if(proprietes.status == "running"){
                if(gameWaiting){
                    getController().waitingControl.startGame();
                    getController().$apply();
                }
                $("#nbTour").html(proprietes.tour);
                alert("Nouveau tour !");
            }
            else if(proprietes.status == "ended"){

                if(listJoueurs[0].pseudo == pseudo){
                    alert("Vous avez gagné ! ");
                }

            }

        }

        //Si on a des informations sur les territoires
        if(infoNames.indexOf("territoires") != -1){
            //On met a jour la liste des territoires
            listTerritoires = arguments.data.territoires;

            //Met a jour la map de bonsai
            if(!gameWaiting)
                majMap();
        }

    }

}

    function deselect(){
        game.sendMessage("deselect", {});
        $("#moveButton").prop("disabled", true);
        $("#nbUnite").val("");
        zoneDepart = zoneArrivee = -1;
    }

  function majMap(){

      var color_territoires = [];

      listTerritoires.forEach(function(t){
          if(t.joueur == "")
            color_territoires.push({id: t.id, color: "#FFFFFF"});
          else{
            listJoueurs.forEach(function(j){
                if(t.joueur == j.pseudo)
                    color_territoires.push({id: t.id, color: j.color});
            });
          }
      });

      game.sendMessage("majMap", {colors: color_territoires});
  }

function connexionPermanente(pseudoSaisi){

    sse = new EventSource('/connexion/'+pseudoSaisi);
    sse.onmessage = function(message) {
        console.log('Arrive message ! ');
        $('#output').append('<li>'+message.data+'</li>');

        onMessageArrive(message.data);
    }

}

function gameStart(){

    $.ajax({
        url: "/gamestart/"+pseudo,
        cache: false
    });

}
