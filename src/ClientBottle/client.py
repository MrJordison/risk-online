#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from gevent import monkey; monkey.patch_all()
from bottle import route, run, static_file, get, post, request, template, response

import re, socket

serverAddress = '127.0.0.1'
serverPort = 8080

def get_data(connexion_client):
    ligne = ''
    while(1):
        car = connexion_client.recv(1).decode()
        if not car:break
        if car == '\n':
            #ligne+=car
            break
        ligne+=car
    return ligne

def format_sse(data):
    #return 'data: {"message":"'+data+'"}\n\n'
    return 'data: {"message":'+data+'}\n\n'

def getArguments(message) :
    separateur = re.compile(r":")
    arguments = separateur.split(message)
    return message

def makeBeginMessage(pseudo) :
    return "pseudo:"+pseudo+":"

#route par defaut
@route('/')
def home():
    return static_file('game.html', 'static/')

#appele lorsque le chef de groupe lance la partie
@route('/gamestart/:pseudo')
def gameStart(pseudo):

    message = (makeBeginMessage(pseudo)+'game:start\n').encode()

    #on envoie au serveur la requete pour demarrer la partie
    connexion_vers_serveur_jeu = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connexion_vers_serveur_jeu.connect((serverAddress, serverPort))
    connexion_vers_serveur_jeu.sendall(message)
    connexion_vers_serveur_jeu.close()

#appele tout le temps
@route('/connexion/:pseudo')
def connexion(pseudo):

    message = (makeBeginMessage(pseudo)+'participate\n').encode()

    response.content_type = "text/event-stream"
    connexion_vers_serveur_jeu = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connexion_vers_serveur_jeu.connect((serverAddress, serverPort))
    connexion_vers_serveur_jeu.sendall(message)

    while (True):
        data = get_data(connexion_vers_serveur_jeu)
        if not data :
            break
        data = format_sse(data)
        print(data)
        yield data
    connexion_vers_serveur_jeu.close()

#appele lorsqu'un joueur fait un mouvement
@route('/position/:pseudo/:zoneDepart/:zoneArrivee/:nbUnite')
def bouger(pseudo, zoneDepart, zoneArrivee, nbUnite):

    message = (makeBeginMessage(pseudo)+'move:'+zoneDepart+':'+zoneArrivee+':'+nbUnite+'\n').encode()

    connexion_vers_serveur_jeu = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connexion_vers_serveur_jeu.connect((serverAddress, serverPort))
    connexion_vers_serveur_jeu.sendall(message)
    connexion_vers_serveur_jeu.close()

@route('/lib/<name>')
def fileLib(name) :
    return static_file(name, 'lib/')

@route('/js/<name>')
def fileJs(name) :
    return static_file(name, 'js/')

@route('/css/<name>')
def fileCss(name) :
    return static_file(name, 'style/')

@route('/template/<name>')
def fileTemplate(name) :
    return static_file(name, "static/")

run(host='localhost', port=8081, server='gevent', debug=True)
