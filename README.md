A faire au préalable : 

- installation de bottle via bash (voir http://bottlepy.org/docs/dev/tutorial.html#installation pour plus de détails):
commandes utilisées pour le faire tourner sous python3 sur ma linux :

sudo apt-get install python3-pip
python3 pip install --upgrade pip
python3 pip install bottle

- Si besoin, voir les cours d'AngularJS à cette adresse: https://www.codecademy.com/learn ou sur OpenClassroom


Liens vers les API:
- BonsaiJS : http://bonsaijs.org/
- AngularJS : https://angularjs.org/
- Bottle : http://bottlepy.org/docs/dev/index.html

